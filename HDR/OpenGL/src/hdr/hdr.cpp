//
//  HDR sample
//
//  This example loads an HDR cubemap from disk and displays it
//  using simple tonemapping and some post processing effects.
//
//  The framebuffer and texture formats can be modified to examine
//  the quality differences.

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <map>
#include <math.h>
#include <GL/glew.h>
#include <GL/glut.h>
#include "cgGL.h"

#include <nvImage.h>
#include <nvMath.h>
#include <nvGlutManipulators.h>
#define NV_REPORT_COMPILE_ERRORS
#include <nvShaderUtils.h>
#include <nvModel.h>
#include <nvGlutWidgets.h>
#include <nvSDKPath.h>

#include "RenderTextureFBO.h"
#include "blur.h"
#include "cubeMap.h"

using std::map;
using namespace nv;

char *image_filename = "media/textures/pisa_new.hdr";
nv::Image image;
GLuint hdr_tex, test_tex, image_tex;

nv::SDKPath sdkPath;

float exposure = 10.0;
float aniso = 2.0;
float blur_width = 3.0; // width of blur kernel
float blur_amount = 0.5;
float effect_amount = 0.2;

enum UIOption {
    OPTION_WIREFRAME,
    OPTION_GLOW,
    OPTION_RAYS,
    OPTION_DRAW_SKYBOX,
    OPTION_BLEND,
    OPTION_ANIMATE,
    OPTION_MANIPULATE_OBJECT,
    OPTION_TEST_TEX,
    OPTION_COUNT,
};
bool options[OPTION_COUNT];
map<char, UIOption> optionKeyMap;


CGcontext context;
CGprofile cg_vprofile, cg_fprofile;

// shaders
CGprogram skybox_vprog, skybox_fprog;
CGprogram object_vprog, object_fprog;
CGprogram downsample_vprog, downsample_fprog;
CGprogram downsample4_vprog, downsample4_fprog;
CGprogram tonemap_fprog;
CGparameter blurAmount_param, effectAmount_param, windowSize_param, exposure_param;
CGparameter model_param, eyePos_param;
CGparameter twoTexelSize_param;
GLuint blurh_fprog = 0, blurv_fprog = 0;

bool keydown[256];

int win_w = 512, win_h = 512;


nv::GlutExamine camera, object;

#define DOWNSAMPLE_BUFFERS 2
#define BLUR_BUFFERS 2
RenderTexture *scene_buffer = 0,*downsample_buffer[DOWNSAMPLE_BUFFERS];
RenderTexture *blur_buffer[BLUR_BUFFERS];
RenderTexture *ms_buffer = 0;
bool fullscreen = 0;

GLuint buffer_format = GL_RGBA16F_ARB;
GLuint texture_format = GL_RGBA16F_ARB;

int modelno = 3;
char *model_filename = "media/models/Cyborg2.obj";
nv::Model *model = 0;

int have_CSAA = false;

void initGL();
void setOrthoProjection(int w, int h);
void setPerspectiveProjection(int w, int h);
void initBlurCode(float blur_width);
void createBuffers(GLenum format);

// draw a quad with texture coordinate for texture rectangle
void drawQuad(int w, int h)
{
    glBegin(GL_QUADS);
    glTexCoord2f(0.0, 1.0);
    glVertex2f(0.0, h);
    glTexCoord2f(1.0, 1.0);
    glVertex2f(w, h);
    glTexCoord2f(1.0, 0.0);
    glVertex2f(w, 0.0);
    glTexCoord2f(0.0, 0.0);
    glVertex2f(0.0, 0.0);
    glEnd();
}

GLuint createCubemapTexture(nv::Image &img, GLint internalformat)
{
    GLuint tex; 
    glGenTextures(1, &tex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, tex);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_GENERATE_MIPMAP_SGIS, GL_TRUE);

    // load face data
    for(int i=0; i<6; i++) {
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
                     internalformat, img.getWidth(), img.getHeight(), 0, 
                     GL_RGB, GL_FLOAT, img.getLevel(0, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i));
    }

    return tex;
}

// function for creating procedural test cubemap
vec3f testFunc(int face, float s, float t)
{
    vec3f col[] = {
        vec3f(1.0f, 0.0f, 0.0f),
        vec3f(0.0f, 1.0f, 1.0f),
        vec3f(0.0f, 1.0f, 0.0f),
        vec3f(1.0f, 0.0f, 1.0f),
        vec3f(0.0f, 0.0f, 1.0f),
        vec3f(1.0f, 1.0f, 0.0f)
    };
    float i = sqrt(s*s + t*t);
    i = powf(2.0f, -(i+1.0f)*16.0f);
    return col[face]*i;
}

// draw cubemap background
void drawSkyBox()
{
    cgGLBindProgram(skybox_vprog);
    cgGLEnableProfile(cg_vprofile);
    cgGLBindProgram(skybox_fprog);
    cgGLEnableProfile(cg_fprofile);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, hdr_tex);
    glTexEnvf(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, 0.0f);

    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();
    nv::matrix4f m = camera.getTransform();
    nv::matrix4f mi = inverse(m);
    glMultMatrixf((float *) &mi);
    glutSolidCube(1.0);
    glPopMatrix();

    glEnable(GL_DEPTH_TEST);
    cgGLDisableProfile(cg_fprofile);
    cgGLDisableProfile(cg_vprofile);
}

void drawModel( const nv::Model *model)
{
    glVertexPointer( model->getPositionSize(), GL_FLOAT, model->getCompiledVertexSize() * sizeof(float), model->getCompiledVertices());
    glNormalPointer( GL_FLOAT, model->getCompiledVertexSize() * sizeof(float), model->getCompiledVertices() + model->getCompiledNormalOffset());
    glEnableClientState( GL_VERTEX_ARRAY);
    glEnableClientState( GL_NORMAL_ARRAY);

    glDrawElements( GL_TRIANGLES, model->getCompiledIndexCount( nv::Model::eptTriangles), GL_UNSIGNED_INT, model->getCompiledIndices( nv::Model::eptTriangles));

    glDisableClientState( GL_VERTEX_ARRAY);
    glDisableClientState( GL_NORMAL_ARRAY);
}

// read from float texture, apply tone mapping, render to regular 8/8/8 display
void toneMappingPass()
{
    // render to window
    scene_buffer->Deactivate();
    setOrthoProjection(win_w, win_h);

    // bind textures
    glActiveTexture(GL_TEXTURE0);
    scene_buffer->Bind();

    glActiveTexture(GL_TEXTURE1);
    blur_buffer[0]->Bind();

    cgGLBindProgram(tonemap_fprog);
    cgGLEnableProfile(cg_fprofile);

    if (options[OPTION_GLOW]) {
		cgGLSetParameter1f(blurAmount_param, blur_amount);
		cgGLSetParameter1f(effectAmount_param, effect_amount);
	} else {
		cgGLSetParameter1f(blurAmount_param, 0.0f);
		cgGLSetParameter1f(effectAmount_param, 0.0f);
	}

	cgGLSetParameter4f(windowSize_param, 2.0/win_w, 2.0/win_h, -1.0, -1.0);
    cgGLSetParameter1f(exposure_param, exposure);

    glDisable(GL_DEPTH_TEST);
    drawQuad(win_w, win_h);

    cgGLDisableProfile(cg_fprofile);

    glutReportErrors();
}

// render scene to float pbuffer
void renderScene()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    if ( options[OPTION_WIREFRAME])
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    else
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glDisable(GL_MULTISAMPLE);

    if ( options[OPTION_DRAW_SKYBOX]) {
        // draw background
        drawSkyBox();
    }

    // draw object
    cgGLBindProgram(object_vprog);
    cgGLEnableProfile(cg_vprofile);

    cgGLBindProgram(object_fprog);
    cgGLEnableProfile(cg_fprofile);

    glMatrixMode( GL_MODELVIEW );
    glLoadIdentity();
    camera.applyTransform();

    cgGLSetMatrixParameterfc(model_param, object.getTransform().get_value());

    // calculate eye position in cubemap space
    nv::vec4f eyePos_eye(0.0, 0.0, 0.0, 1.0), eyePos_model;
    nv::matrix4f view = camera.getTransform();
    nv::matrix4f viewInv = inverse(view);
    eyePos_model = viewInv * eyePos_eye;
    cgGLSetParameter3f(eyePos_param, eyePos_model[0], eyePos_model[1], eyePos_model[2]);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_CUBE_MAP, hdr_tex);

    // bias LOD to blur cubemap a little
    glTexEnvf(GL_TEXTURE_FILTER_CONTROL_EXT, GL_TEXTURE_LOD_BIAS_EXT, 2.0f);

    glEnable(GL_CULL_FACE);

    if ( options[OPTION_BLEND]) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glDisable(GL_DEPTH_TEST);
    } else {
        glEnable(GL_DEPTH_TEST);
    }

    switch(modelno) {
    case 0:
        glutSolidSphere(1.0, 40, 40);
        break;
    case 1:
        glutSolidTetrahedron();
        break;
    case 2:
        glutSolidCube(1.0);
        break;
    case 3:
        glCallList(1);
        break;
    case 4:
        break;
    }

    glDisable(GL_BLEND);
    glDisable(GL_CULL_FACE);
    glDisable(GL_MULTISAMPLE);
    cgGLDisableProfile(cg_fprofile);
    cgGLDisableProfile(cg_vprofile);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glutReportErrors();
}

// downsample image 2x in each dimension
void downsample(RenderTexture *src, RenderTexture *dest)
{
    dest->Activate();
    setOrthoProjection(dest->GetWidth(), dest->GetHeight());

    cgGLBindProgram(downsample_vprog);
    cgGLEnableProfile(cg_vprofile);
    cgGLBindProgram(downsample_fprog);
    cgGLEnableProfile(cg_fprofile);
    glActiveTexture(GL_TEXTURE0);
    src->Bind();
    drawQuad(dest->GetWidth(), dest->GetHeight());
    src->Release();
    cgGLDisableProfile(cg_fprofile);
    cgGLDisableProfile(cg_vprofile);

    dest->Deactivate();
}

// downsample image 4x in each dimension
void downsample4(RenderTexture *src, RenderTexture *dest)
{
    dest->Activate();
    setOrthoProjection(dest->GetWidth(), dest->GetHeight());

    cgGLBindProgram(downsample4_vprog);
    cgGLEnableProfile(cg_vprofile);
    cgGLBindProgram(downsample4_fprog);
    cgGLSetParameter2f(twoTexelSize_param, 2.0 / src->GetWidth(), 2.0 / src->GetWidth());
    cgGLEnableProfile(cg_fprofile);
    glActiveTexture(GL_TEXTURE0);
    src->Bind();
    drawQuad(dest->GetWidth(), dest->GetHeight());
    src->Release();
    cgGLDisableProfile(cg_fprofile);
    cgGLDisableProfile(cg_vprofile);

    dest->Deactivate();
}

//
//  generic full screen processing function
//
//////////////////////////////////////////////////////////////////////
void run_pass(GLuint prog, RenderTexture *src, RenderTexture *dest)
{
    dest->Activate();
    glBindProgramARB(GL_FRAGMENT_PROGRAM_ARB, prog);
    glEnable(GL_FRAGMENT_PROGRAM_ARB);
    glActiveTexture(GL_TEXTURE0);
    src->Bind();
    drawQuad(dest->GetWidth(), dest->GetHeight());
    src->Release();
    glDisable(GL_FRAGMENT_PROGRAM_ARB);
    dest->Deactivate();
}

//
//  generic full screen processing function
//
//////////////////////////////////////////////////////////////////////
void run_pass(CGprogram prog, RenderTexture *src, RenderTexture *dest)
{
    dest->Activate();
    cgGLBindProgram(prog);
    cgGLEnableProfile(cg_fprofile);
    glActiveTexture(GL_TEXTURE0);
    src->Bind();
    drawQuad(dest->GetWidth(), dest->GetHeight());
    src->Release();
    cgGLDisableProfile(cg_fprofile);
    dest->Deactivate();
}

//
//  function used to apply the gaussian blur
//
//////////////////////////////////////////////////////////////////////
void glow(RenderTexture *src, RenderTexture *dest, RenderTexture *temp)
{
    setOrthoProjection(dest->GetWidth(), dest->GetHeight());

    // horizontal blur
    run_pass(blurh_fprog, src, temp);

    // vertical blur
    run_pass(blurv_fprog, temp, dest);
}

//
//
//////////////////////////////////////////////////////////////////////
inline void updateButtonState( const nv::ButtonState &bs, nv::GlutManipulator &manip, int button) {
    int modMask = 0;

    if (bs.state & nv::ButtonFlags_Alt) modMask |= GLUT_ACTIVE_ALT;
    if (bs.state & nv::ButtonFlags_Shift) modMask |= GLUT_ACTIVE_SHIFT;
    if (bs.state & nv::ButtonFlags_Ctrl) modMask |= GLUT_ACTIVE_CTRL;

    if (bs.state & nv::ButtonFlags_End)
        manip.mouse( button, GLUT_UP, modMask, bs.cursor.x, win_h - bs.cursor.y);
    if (bs.state & nv::ButtonFlags_Begin)
        manip.mouse( button, GLUT_DOWN, modMask, bs.cursor.x, win_h - bs.cursor.y);
}


//
//  display callback function
//
//////////////////////////////////////////////////////////////////////
void display()
{	
	scene_buffer->Activate();

    setPerspectiveProjection(win_w, win_h);
    renderScene();


    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);

    // blur pass
    if ( options[OPTION_GLOW] || options[OPTION_RAYS]) {
        // down sample 2x twice ot get a 4x4 mean filter
		downsample(scene_buffer, downsample_buffer[0]);
		downsample(downsample_buffer[0], downsample_buffer[1]);

        //perform a separable blurr filter for the glow 
        glow(downsample_buffer[1], blur_buffer[0], blur_buffer[1]);
    }
    // tone mapping
    toneMappingPass();

   
    glutReportErrors();
    glutSwapBuffers();
}

void testKeys()
{
    if (keydown[']']) {
        exposure *= 1.01;
        glutPostRedisplay();
    }
    if (keydown['[']) {
        exposure /= 1.01;
        glutPostRedisplay();
    }
}

//
//  idle callback function
//
//////////////////////////////////////////////////////////////////////
void idle()
{
    static float last_time;
    float time = glutGet(GLUT_ELAPSED_TIME) / 1000.0;
    float dt = time - last_time;
    last_time = time;

    if ( options[OPTION_ANIMATE]) {
		camera.idle();
    }

    testKeys();
    glutPostRedisplay();
}

//
// keypress callback
//
//////////////////////////////////////////////////////////////////////
void key(unsigned char k, int x, int y)
{
   
    k = tolower(k);

    if (optionKeyMap.find(k) != optionKeyMap.end())
        options[optionKeyMap[k]] = ! options[optionKeyMap[k]];

    switch (k) {
    case '\033':
    case 'q':
        if (fullscreen)
            glutLeaveGameMode();
        exit(0);
        break;

    case '+':
    case '=':
        exposure*=2;
        break;

    case '_':
    case '-':
        exposure*=0.5;
        break;

    case '.':
        blur_amount += 0.1;
        if (blur_amount > 2.0) blur_amount = 2.0;
        break;
    case ',':
        blur_amount -= 0.1;
        if (blur_amount < 0.0) blur_amount = 0.0;
        break;

    case 'r':
		if ( options[OPTION_RAYS]) {
			effect_amount = 0.1;
		} else {
			effect_amount = 0.0;
		}
        break;

    case 'm':
        modelno = (modelno + 1) % 4;
        break;

    default:
        keydown[k] = 1;
        break;
    }
    printf("exposure = %f, blur = %f", exposure, blur_amount);
    glutPostRedisplay();
}


void keyUp(unsigned char key, int x, int y)
{
    keydown[key] = 0;
}


void setOrthoProjection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, w, 0, h, -1.0, 1.0);
    glMatrixMode(GL_TEXTURE);
    glLoadIdentity();
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, w, h);
}

void setPerspectiveProjection(int w, int h)
{
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(60.0, (float) w / (float) h, 0.1, 10.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    glViewport(0, 0, w, h);
}


void reshape(int w, int h)
{
   
    win_w = w;
    win_h = h;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, win_w, 0, win_h, -1.0, 1.0);

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    glViewport(0, 0, win_w, win_h);

    createBuffers(buffer_format);
    initBlurCode(blur_width);

    camera.reshape(w, h);
    object.reshape(w, h);
}

void cgErrorCallback(void)
{
    CGerror lastError = cgGetError();
    if(lastError) {
        const char *listing = cgGetLastListing(context);
        printf("%s\n", cgGetErrorString(lastError));
        printf("%s\n", listing);
        exit(-1);
    }
}

CGprogram loadProgram(CGcontext context, char *path, char *filename, char *entry, CGprofile profile)
{
    char fullpath[256];
    sprintf(fullpath, "%s/%s", path, filename);
    std::string resolvedPath;

    CGprogram program = NULL;

    if (sdkPath.getFilePath( fullpath, resolvedPath)) {
        program = cgCreateProgramFromFile(context, CG_SOURCE, resolvedPath.c_str(), profile, entry, NULL);
        if (!program) {
            fprintf(stderr, "Error creating program '%s'\n", fullpath);
        }
        cgGLLoadProgram(program);
    } else {
        fprintf(stderr, "Failed to locate program '%s'\n", fullpath);
    }

    return program;
}

void initCg()
{
    cgSetErrorCallback(cgErrorCallback);
    context = cgCreateContext();

    cg_vprofile = cgGLGetLatestProfile(CG_GL_VERTEX);
    cg_fprofile = cgGLGetLatestProfile(CG_GL_FRAGMENT);
    
    char *path = "src/hdr/shaders/"; //path relative to the SDK root
    downsample_vprog = loadProgram(context, path, "downsample.cg", "downsample_vp", cg_vprofile);
    downsample_fprog = loadProgram(context, path, "downsample.cg", "downsample_fp", cg_fprofile);

    downsample4_vprog = loadProgram(context, path, "downsample.cg", "downsample4_vp", cg_vprofile);
    downsample4_fprog = loadProgram(context, path, "downsample.cg", "downsample4_fp", cg_fprofile);
    twoTexelSize_param = cgGetNamedParameter(downsample4_vprog, "twoTexelSize");

    tonemap_fprog = loadProgram(context, path, "tonemap.cg", "main", cg_fprofile);

    blurAmount_param = cgGetNamedParameter(tonemap_fprog, "blurAmount");
	effectAmount_param = cgGetNamedParameter(tonemap_fprog, "effectAmount");
	windowSize_param = cgGetNamedParameter(tonemap_fprog, "windowSize");
    exposure_param   = cgGetNamedParameter(tonemap_fprog, "exposure");

    object_vprog = loadProgram(context, path, "object.cg", "object_vp", cg_vprofile);
    model_param         = cgGetNamedParameter(object_vprog, "model");
    eyePos_param        = cgGetNamedParameter(object_vprog, "eyePos");

    object_fprog = loadProgram(context, path, "object.cg", "object_fp", cg_fprofile);

    skybox_vprog = loadProgram(context, path, "skybox.cg", "skybox_vp", cg_vprofile);
    skybox_fprog = loadProgram(context, path, "skybox.cg", "skybox_fp", cg_fprofile);
}

void createBuffers(GLenum format)
{
    if (scene_buffer) {
        delete scene_buffer;
    }
    for(int i=0; i<DOWNSAMPLE_BUFFERS; i++) {
        if (downsample_buffer[i])
            delete downsample_buffer[i];
    }
    for(int i=0; i<BLUR_BUFFERS; i++) {
        if (blur_buffer[i])
            delete blur_buffer[i];
    }

    GLenum target = GL_TEXTURE_2D;

    // create float pbuffers
    scene_buffer = new RenderTexture(win_w, win_h, target);
    scene_buffer->InitColor_Tex(0, format);
    glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    scene_buffer->InitDepth_RB();


    int w = win_w;
    int h = win_h;
    for(int i=0; i<DOWNSAMPLE_BUFFERS; i++) {
        w /= 2;
        h /= 2;
        downsample_buffer[i] = new RenderTexture(w, h, target);
        downsample_buffer[i]->InitColor_Tex(0, format);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }

    // blur pbuffers
    for(int i=0; i<BLUR_BUFFERS; i++) {
        blur_buffer[i] = new RenderTexture(win_w / 4, win_h / 4, target);
        blur_buffer[i]->InitColor_Tex(0, format);
        glTexParameteri(target, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(target, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    }
}

// get extension pointers
void getExts()
{
    glewInit();

    if (!glewIsSupported(
        "GL_VERSION_2_0 "
        "GL_ARB_fragment_program "
        "GL_ARB_vertex_program "
        "GL_ARB_texture_float "
        "GL_ARB_color_buffer_float "
        "GL_EXT_framebuffer_object "
        ))
    {
        fprintf(stderr, "Error - required extensions were not supported\n");
        exit(-1);
    }

    if (glewIsSupported("GL_EXT_packed_float")) {
        buffer_format = GL_R11F_G11F_B10F_EXT;
    }

    if (glewIsSupported("GL_EXT_texture_shared_exponent")) {
        texture_format = GL_RGB9_E5_EXT;
    }

    have_CSAA = glewIsSupported("GL_NV_framebuffer_multisample_coverage");
}

void initGL()
{
    getExts();
    initCg();
    createBuffers(buffer_format);

    glutReportErrors();
}

void initBlurCode(float blur_width)
{
    // delete old programs
    if (blurh_fprog) {
        glDeleteProgramsARB(1, &blurh_fprog);
    }
    if (blurh_fprog) {
        glDeleteProgramsARB(1, &blurh_fprog);
    }

    // generate weights for gaussian blur
    float *weights;
    int width;
    weights = generateGaussianWeights(blur_width, width);

    // generate blur fragment programs
    blurh_fprog = generate1DConvolutionFP_filter(weights, width, false, true, win_w / 2, win_h / 2);
    blurv_fprog = generate1DConvolutionFP_filter(weights, width, true, true, win_w / 2, win_h / 2);

    delete [] weights;
}

void mainMenu(int i)
{
    key((unsigned char) i, 0, 0);
}


void bufferFormatMenu(int i)
{
    buffer_format = i;
    createBuffers(buffer_format);
    glutPostRedisplay();
}



void anisoMenu(int i)
{
    aniso = (float) i;
    glBindTexture(GL_TEXTURE_CUBE_MAP, hdr_tex);
    glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, aniso);
    glutPostRedisplay();
}


void parseArgs(int argc, char **argv)
{
    for(int i=1; i<argc; i++) {
        if (strcmp("-image", argv[i])==0) {
            image_filename = argv[++i];

        } else if (strcmp("-window", argv[i])==0) {
            win_w = atoi(argv[++i]);
            win_h = atoi(argv[++i]);

        } else if (strcmp("-exposure", argv[i])==0) {
            exposure = atof(argv[++i]);

        } else if (strcmp("-blurwidth", argv[i])==0) {
            blur_width = atoi(argv[++i]);

        } else if (strcmp("-model", argv[i])==0) {
            model_filename = argv[++i];

        } else if (strcmp("-fullscreen", argv[i])==0) {
            fullscreen = true;

        }else {
            fprintf(stderr, "Unrecognized option '%s'\n", argv[i]);
            printf("usage: %s [-image image.hdr] [-model model.obj] [-window width height] [-exposure f] [-blurwidth f] [-blurscale d]\n", argv[0]);
        }
    }
}

int main(int argc, char **argv)
{
    std::string pathString;
    glutInit(&argc, argv);
    parseArgs(argc, argv);

    char displayString[256];
    sprintf(displayString, "double rgb~8 depth~16");

    if (fullscreen) {
        glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);
        char gamemode[256];
        sprintf(gamemode, "%dx%d:%d", win_w, win_h, 32);
        glutGameModeString(gamemode);
        int win = glutEnterGameMode();

    } else {
        // windowed
        glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH | GLUT_DOUBLE);

        glutInitWindowSize(win_w, win_h);
        (void) glutCreateWindow("HDR");
    }

    glutDisplayFunc(display);
    glutIdleFunc(idle);
    glutKeyboardFunc(key);
    glutKeyboardUpFunc(keyUp);
    glutReshapeFunc(reshape);

    camera.setDollyActivate( GLUT_LEFT_BUTTON, GLUT_ACTIVE_CTRL);
    camera.setPanActivate( GLUT_LEFT_BUTTON, GLUT_ACTIVE_SHIFT);
    camera.setDollyPosition( -3.0f);

	// set camera rotating
	nv::ExamineManipulator &manip = (nv::ExamineManipulator &) camera.getManipulator();
	manip.getIncrement().set_value(nv::vec3f(0.0, 1.0, 0), 0.001);

    optionKeyMap['w'] = OPTION_WIREFRAME;
    options[OPTION_WIREFRAME] = false;

    optionKeyMap[' '] = OPTION_ANIMATE;
    options[OPTION_ANIMATE] = true;

    optionKeyMap['g'] = OPTION_GLOW;
    options[OPTION_GLOW] = true;

    optionKeyMap['r'] = OPTION_RAYS;
    options[OPTION_RAYS] = true;

    optionKeyMap['s'] = OPTION_DRAW_SKYBOX;
    options[OPTION_DRAW_SKYBOX] = true;

    optionKeyMap['o'] = OPTION_MANIPULATE_OBJECT;
    options[OPTION_MANIPULATE_OBJECT] = false;

    optionKeyMap['t'] = OPTION_TEST_TEX;
    options[OPTION_TEST_TEX] = false;

    printf( "HDR - sample showing high dynamic range imaging\n");
    printf( "   q/[ESC]    - Quit the app\n");
    printf( "   [SPACE]    - Toggle animation on/off\n");
    printf( "      w       - Toggle displaying the object in wireframe\n");
    printf( "      g       - Toggle the glow pass\n");
    printf( "      r       - Toggle the rays effect\n");
    printf( "      s       - Toggle drawing the skybox\n");
    printf( "      o       - Toggle between controlling the object and controlling the viewer\n");
    printf( "      +       - Double exposure\n");
    printf( "      -       - Half exposure\n");
    printf( "      ]       - Increment exposure\n");
    printf( "      [       - Decrement exposure\n");
    printf( "      .       - Increase blur amount\n");
    printf( "      ,       - Decrease blur amount\n");
    printf( "      m       - Change model\n");
    
    if ( sdkPath.getFilePath(image_filename, pathString)) {
        if (!image.loadImageFromFile(pathString.c_str())) {
            fprintf(stderr, "Error loading image file '%s'\n", pathString.c_str());
            exit(-1);
        }
        if (!image.convertCrossToCubemap()) {
            fprintf(stderr, "Error converting image to cubemap\n");
            exit(-1);        
        };
    }
    else {
        fprintf(stderr, "Filed to find image '%s'\n", image_filename);
        exit(-1);        
    }

    for(int i=0; i<DOWNSAMPLE_BUFFERS; i++)
        downsample_buffer[0] = 0;
    for(int i=0; i<BLUR_BUFFERS; i++)
        blur_buffer[0] = 0;

    initGL();
    initBlurCode(blur_width);

    image_tex = createCubemapTexture(image, texture_format);
    test_tex = createCubemapTextureFromFunc(256, texture_format, testFunc);
    hdr_tex = image_tex;

    // create the model and make it ready for display
    model = new nv::Model;
    if ( sdkPath.getFilePath(model_filename, pathString)) {
        if (model->loadModelFromFile(pathString.c_str())) {

            // compute normal
            model->computeNormals();

            // get the bounding box to help place the model on-screen
            model->rescale(1.0f);

            // make the model efficient for rendering with vertex arrays
            model->compileModel( nv::Model::eptAll);

            glNewList(1, GL_COMPILE);
            drawModel(model);
            glEndList();

        } else {
            fprintf(stderr, "Error loading model '%s'\n", pathString.c_str());
            delete model;
            model = 0;
        }
    } else {
        fprintf(stderr, "Unable to locate model '%s'\n", model_filename);
        delete model;
        model = 0;
    }

  
    glutMainLoop();
    return 0;
}
